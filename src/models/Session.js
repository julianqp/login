var mongoose = require("mongoose");
const uniqueValidator = require("mongoose-unique-validator");
var Schema = mongoose.Schema;

var sessionSchema = new mongoose.Schema({
  user: String,
  token: {
    type: String,
    unique: true
  },
  date: Date
});
sessionSchema.plugin(uniqueValidator);
const Session = mongoose.model("Session", sessionSchema);

module.exports = Session;
