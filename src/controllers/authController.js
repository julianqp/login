const nodemailer = require("nodemailer");
const jwt = require("jsonwebtoken");
const options = require("../config/constants");
const User = require("../models/User");
const Session = require("../models/Session");
const TemRegistro = require("../templates/template-registro");
const TemReset = require("../templates/template-reset");
const validateToken = require("../utils/util");

module.exports = {
  // https://docs.mongodb.com/v3.0/reference/operator/query/text/
  login: async function(req, res) {
    const username = req.body.user;
    const password = req.body.password;

    const result = await User.findOne({ user: username, pass: password });

    if (result === null) {
      return res.status(403).send({
        error: "usuario o contraseña inválidos"
      });
    }

    const tokenData = {
      username: username
    };

    const token = jwt.sign(tokenData, options.clave, {
      expiresIn: 120 * 30 // expires in 24 hours
    });

    await Session.create({
      user: username,
      token: token,
      date: new Date()
    });

    return res.send({
      token
    });
  },
  logout: async function(req, res) {
    try {
      const token = await validateToken(req.headers.authorization);
      const user = req.params.user;
      let result = null;

      if (
        token.result &&
        token.data &&
        token.data.username &&
        token.data.username === user
      ) {
        try {
          result = await Session.deleteOne({
            token: req.headers.authorization,
            user: user
          });
        } catch (e) {
          return res.status(403).send({
            error: JSON.stringify(e)
          });
        }
        if (result.n === 0) {
          return res.status(403).send({
            error: "Sesion no encontrada"
          });
        }
        return res.send({
          result: "Sesion cerrada con exito"
        });
      }
      return res.status(403).send({
        error: "Falta token"
      });
    } catch (error) {
      return res.status(403).send({
        error: JSON.stringify(error)
      });
    }
  },
  register: async function(req, res) {
    const username = req.body.user;
    const password = req.body.password;
    const email = req.body.email;

    await User.create(
      { user: username, pass: password, email },
      (error, doc) => {
        if (error) {
          return res.status(403).json({ error: error.message });
        }

        const transporter = nodemailer.createTransport({
          service: "gmail",
          auth: {
            user: options.email,
            pass: options.pass
          },
          debug: true
        });

        const mailOptions = {
          from: "equipo.is2.16@gmail.com",
          to: email,
          subject: "Prueba",
          html: TemRegistro()
        };

        transporter.sendMail(mailOptions, function(error, info) {
          if (error) {
            res.status(403).send({ error: JSON.stringify(error) });
          } else {
            res.status(201).send(doc);
          }
        });
      }
    );
  },
  reset: async function(req, res) {
    try {
      const email = req.body.email;
      const user = req.body.user;

      await User.findOne({ email: email, user: user }, (error, doc) => {
        if (error) {
          return res.status(403).json({ error: error.message });
        } else if (doc === null) {
          return res.status(403).json({ error: "No existe el usuario" });
        } else {
          const transporter = nodemailer.createTransport({
            service: "gmail",
            auth: {
              user: options.email,
              pass: options.pass
            },
            debug: true
          });

          const mailOptions = {
            from: "equipo.is2.16@gmail.com",
            to: email,
            subject: "Prueba",
            html: TemReset()
          };

          transporter.sendMail(mailOptions, function(error, info) {
            if (error) {
              res.status(403).send({ error: "Correo no enviado" });
            } else {
              res.send({
                result: "Correo enviado"
              });
            }
          });
        }
      });
    } catch (error) {}
  },
  remove: async function(req, res) {
    let result = null;
    let response = {
      result: null,
      error: "",
      status: 0
    };
    try {
      const token = req.headers.authorization;
      const user = req.params.user;
      const validator = await validateToken(token);

      if (validator.result) {
        if (validator.data.username === user) {
          try {
            result = await User.deleteOne({
              user: user
            });
            await Session.deleteMany({ user: user });
            if (result.n > 0) {
              response.result = true;
              response.status = 200;
            } else {
              response.result = false;
              response.status = 403;
              response.error = "No se ha podido borrar";
            }
          } catch (e) {
            return res.status(403).send({
              error: JSON.stringify(e)
            });
          }
        } else {
          response.result = false;
          response.status = 403;
          response.error = "Token inválido para el usuario";
        }
      } else {
        response.result = false;
        response.status = 403;
        response.error = JSON.parse(validator.error);
      }

      res
        .status(response.status)
        .json({ result: response.result, error: response.error });
    } catch (error) {
      response.result = false;
      response.status = 403;
      response.error = error;
      res
        .status(response.status)
        .json({ result: response.result, error: response.error });
    }
  },
  isValid: async function(req, res) {
    try {
      const token_ = await validateToken(req.headers.authorization);
      const user = req.params.user;
      const token = req.headers.authorization.replace("Bearer ", "");
      let result = null;
      let resultUser = null;
      let response = {
        error: "Token inválido para el usuario",
        status: 403,
        result: null
      };

      if (token_.result && token_.data.username === user) {
        result = await Session.findOne({ user: user, token: token });
        if (result !== null) {
          resultUser = await User.findOne({ user: user });
          if (resultUser !== null) {
            response.result = resultUser;
            response.status = 200;
            response.error = null;
          } else {
            response.error = "Usuario no encontrado.";
            response.status = 403;
          }
        } else {
          response.error = "Token o usuario invalido";
          response.status = 403;
        }
      }
      return res
        .status(response.status)
        .send({ result: response.result, error: response.error });
    } catch (e) {
      return res.status(403).send({ result: null, error: e });
    }
  }
};

/*
 *secure: function(req, res) {
    var token = req.headers["authorization"];
    if (!token) {
      res.status(401).send({
        error: "Es necesario el token de autenticación"
      });
      return;
    }

    token = token.replace("Bearer ", "");

    jwt.verify(token, options.clave, function(err, user) {
      if (err) {
        res.status(401).send({
          error: "Token inválido"
        });
      } else {
        res.send({
          message: "Awwwww yeah!!!!"
        });
      }
    });
  }, 
 */
