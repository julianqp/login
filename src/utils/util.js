const jwt = require("jsonwebtoken");
const options = require("../config/constants");

const validateToken = async token => {
  const token_ = token.replace("Bearer ", "");
  const promise = new Promise((resolve, reject) => {
    jwt.verify(token_, options.clave, (error, customer) => {
      if (error) {
        reject(error);
      } else {
        resolve(customer);
      }
    });
  });
  try {
    const result = await promise;
    return { result: true, data: result, error: null };
  } catch (e) {
    return { result: false, error: JSON.stringify(e), data: null };
  }
};

module.exports = validateToken;
