var router = require("express").Router();
var authController = require("../controllers/authController");
var bodyParser = require("body-parser");
var jsonParser = bodyParser.json();

router.post("/login", jsonParser, function(req, res) {
  authController.login(req, res);
});

router.post("/register", jsonParser, (req, res) => {
  authController.register(req, res);
});

router.get("/secure", (req, res) => {
  authController.secure(req, res);
});

router.post("/reset", jsonParser, (req, res) => {
  authController.reset(req, res);
});

router.delete("/logout/user/:user", (req, res) => {
  authController.logout(req, res);
});

router.delete("/delete/user/:user", (req, res) => {
  authController.remove(req, res);
});

router.get("/user/:user", (req, res) => {
  authController.isValid(req, res);
});

module.exports = router;
