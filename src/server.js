var express = require("express"); //llamamos a Express
var app = express();
var bodyParser = require("body-parser");
var cors = require("cors");
require("./db");

var port = process.env.PORT || 3002; // establecemos nuestro puerto

app.use(cors());

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json({ "Content-Type": "application/*+json" }));

// nuestra ruta irá en http://localhost:3001/api
// es bueno que haya un prefijo, sobre todo por el tema de versiones de la API
var router = require("./routes");
app.use("/api", router);

//arrancamos el servidor
app.listen(port);
console.log("API escuchando en el puerto " + port);

module.exports = app;
