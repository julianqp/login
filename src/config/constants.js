require("dotenv").config("login");
const options = {
  db: "mongodb://localhost/auth",
  clave: "authlogin",
  email: process.env.EMAIL,
  pass: process.env.PASS
};

if (process.env.NODE_ENV === "test") {
  options.db = "mongodb://localhost/testauth";
}

module.exports = options;
