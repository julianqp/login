module.exports = () => {
  return `
  <html>
  <head>
    <style>
      h1 {
        text-align: center;
      }
      .texto {
        margin: 5px 15px 5px 15px;
      }
      .centrado {
        text-align: center;
      }
      .con-boton {
        display: flex;
        justify-content: center;
        width: 100%;
      }
      .boton {
        background: transparent;
        color: #0099cc;
        border: 2px solid #0099cc;
        border-radius: 6px;
        border: none;
        color: white;
        padding: 16px 32px;
        text-align: center;
        display: inline-block;
        font-size: 16px;
        margin: 4px 2px;
        -webkit-transition-duration: 0.4s; /* Safari */
        transition-duration: 0.4s;
        cursor: pointer;
        text-decoration: none;
        text-transform: uppercase;
        background-color: white;
        color: black;
        border: 2px solid #008cba;
      }
      .boton:hover {
        background-color: #008cba;
        color: white;
      }
      .footer {
        margin: 5px 15px 5px 15px;
      }
    </style>
  </head>
  <body>
    <h1>Restablecer contraseña usuario</h1>
    <div class="texto">
      <p class="centrado">Usuario</p>
      <p class="centrado">
        Para restablecer la contraseña, pulse el botón y siga las instrucciónes
        que aparecerán a continuación. <b>El tiempo límite son dos horas.</b>
      </p>
    </div>
    <div class="con-boton">
      <button class="boton">
        Restablecer
      </button>
    </div>
    <br />
    <br />
    <div class="footer">
      <p class="centrado">
        Si usted no ha solicitado el cambio de contraseña, pulse
        <a href="https:www.google.es" target="_blank">aquí</a>.
      </p>
    </div>
  </body>
</html>
    `;
};
