const request = require("supertest");
const app = require("../src/server");

let token;

describe("Test correcto", () => {
  test("Register OK", async () => {
    const res = await request(app)
      .post("/api/auth/register")
      .send({
        email: "equipo.is2.16@gmail.com",
        password: "prueba1",
        user: "Prueba1"
      });
    expect(res.statusCode).toEqual(201);
    expect(res.body).toHaveProperty("_id");
  });

  test("Login OK", async () => {
    const res = await request(app)
      .post("/api/auth/login")
      .send({
        password: "prueba1",
        user: "Prueba1"
      });
    expect(res.statusCode).toEqual(200);
    token = res.body.token;
    expect(res.body).toHaveProperty("token");
  });

  test("Reset OK", async () => {
    const res = await request(app)
      .post("/api/auth/reset")
      .send({
        email: "equipo.is2.16@gmail.com",
        user: "Prueba1"
      });
    expect(res.statusCode).toEqual(200);
    expect(res.body).toHaveProperty("result");
  });

  test("Remove U OK", async () => {
    const res = await request(app)
      .delete("/api/auth/delete/user/Prueba1")
      .set({ Authorization: token });
    expect(res.statusCode).toEqual(200);
    expect(res.body).toHaveProperty("result");
  });

  test("Register OK", async () => {
    const res = await request(app)
      .post("/api/auth/register")
      .send({
        email: "equipo.is2.16@gmail.com",
        password: "prueba1",
        user: "Prueba1"
      });
    expect(res.statusCode).toEqual(201);
    expect(res.body).toHaveProperty("_id");
  });

  test("Login OK", async () => {
    const res = await request(app)
      .post("/api/auth/login")
      .send({
        password: "prueba1",
        user: "Prueba1"
      });
    expect(res.statusCode).toEqual(200);
    token = res.body.token;
    expect(res.body).toHaveProperty("token");
  });

  test("Validadcion OK", async () => {
    const res = await request(app)
      .get("/api/auth/user/Prueba1")
      .set({ Authorization: token });
    expect(res.statusCode).toEqual(200);
    expect(res.body.result).not.toBeNull();
  });

  test("Remove S OK", async () => {
    const res = await request(app)
      .delete("/api/auth/logout/user/Prueba1")
      .set({ Authorization: token });
    expect(res.statusCode).toEqual(200);
    expect(res.body).toHaveProperty("result");
  });
});

describe("Test 403", () => {
  test("Register KO", async () => {
    const res = await request(app)
      .post("/api/auth/register")
      .send({
        email: "equipo.is2.16@gmail.com",
        password: "prueba1",
        user: "Prueba1"
      });
    expect(res.statusCode).toEqual(403);
    expect(res.body).toHaveProperty("error");
  });

  test("Remove U OK", async () => {
    const res = await request(app)
      .delete("/api/auth/delete/user/Prueba1")
      .set({ Authorization: token });
    expect(res.statusCode).toEqual(200);
    expect(res.body).toHaveProperty("result");
  });

  test("Validadcion KO", async () => {
    const res = await request(app)
      .get("/api/auth/user/Prueba")
      .set({ Authorization: token });
    expect(res.statusCode).toEqual(403);
    expect(res.body.error).not.toBeNull();
  });

  test("Validadcion KO", async () => {
    const res = await request(app)
      .get("/api/auth/user/Prueba1")
      .set({ Authorization: "ajdfjakjsdkfjk" });
    expect(res.statusCode).toEqual(403);
    expect(res.body.error).not.toBeNull();
  });

  test("Login KO", async () => {
    const res = await request(app)
      .post("/api/auth/login")
      .send({
        password: "prueba2",
        user: "Prueba1"
      });
    expect(res.statusCode).toEqual(403);
    expect(res.body).toHaveProperty("error");
  });

  test("Reset KO", async () => {
    const res = await request(app)
      .post("/api/auth/reset")
      .send({
        email: "equipo.is24.16@gmail.com",
        user: "Prueba2"
      });
    expect(res.statusCode).toEqual(403);
    expect(res.body).toHaveProperty("error");
  });

  test("Remove U KO", async () => {
    const res = await request(app)
      .delete("/api/auth/delete/user/Prueba1")
      .set({ Authorization: "asdkfjkahsjdfja" });
    expect(res.statusCode).toEqual(403);
    expect(res.body).toHaveProperty("error");
  });

  test("Remove U KO", async () => {
    const res = await request(app)
      .delete("/api/auth/delete/user/Prueba2")
      .set({ Authorization: token });
    expect(res.statusCode).toEqual(403);
    expect(res.body).toHaveProperty("error");
  });

  test("Remove S KO", async () => {
    const res = await request(app)
      .delete("/api/auth/logout/user/Prueba2")
      .set({ Authorization: token });
    expect(res.statusCode).toEqual(403);
    expect(res.body).toHaveProperty("error");
  });

  test("Remove S KO", async () => {
    const res = await request(app)
      .delete("/api/auth/logout/user/Prueba1")
      .set({ Authorization: "kdjfaksdkfjaksjkdjfajslkdfj" });
    expect(res.statusCode).toEqual(403);
    expect(res.body).toHaveProperty("error");
  });
});
